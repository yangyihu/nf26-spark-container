# Docker Compose File for NF26 Project

One container for spark

Simply run
```
    docker-compose build #wait, this takes time
    docker-compose up | grep spark
```
Remember to search for the jupyter notebook token to login to the notebook

## Some configuration
Notebook is running on the 8888 port, simply hit localhost:8888

The script folder is map to the spark's container : /script folder
## Examples
Find examples in script folder to show you the usage of pyspark

## FAQ
Q: I want to pip more packages
```
    docker exec -it  bash
    #than pip3 whatever you want
```
Q: I want to have a clean build for the container
```
    docker-compose down #and than, this will clean all the related containers'image
    docker-compose up
```

## This is only for debug purpose :0
