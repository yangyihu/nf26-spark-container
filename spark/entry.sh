export SPARK_HOME=/spark/
export PATH=$SPARK_HOME/bin:$PATH
export PYSPARK_DRIVER_PYTHON=jupyter
export PYSPARK_DRIVER_PYTHON_OPTS='notebook'

jupyter notebook --ip=0.0.0.0 --port=8888 --allow-root --no-browser